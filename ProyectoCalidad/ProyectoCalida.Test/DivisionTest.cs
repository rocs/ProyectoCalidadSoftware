﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProyectoCalidad;

namespace ProyectoCalida.Test
{
    [TestClass]
    public class DivisionTest
    {
        [TestMethod]
        public void DivisionConNumerosValidos()
        {
            //Arrage
            const int dividendo = 4;
            const int divisor = 2;
            const int esperado = 2;

            //Act
            double optenido = Utilidades.division(dividendo, divisor);

            //Assert
            Assert.AreEqual(esperado, optenido);
        }

        [TestMethod]
        public void DivisionConNumerosNegativos()
        {
            //Arrage
            const int dividendo = -4;
            const int divisor = 2;
            const int esperado = -2;

            //Act
            double optenido = Utilidades.division(dividendo, divisor);

            //Assert
            Assert.AreEqual(esperado, optenido);
        }

        [TestMethod]
        public void DivisionConCero()
        {
            //Arrage
            const int dividendo = -4;
            const int divisor = 0;
            const int esperado = 0;

            //Act
            double optenido = Utilidades.division(dividendo, divisor);

            //Assert
            Assert.AreEqual(esperado, optenido);
        }
    }
}
