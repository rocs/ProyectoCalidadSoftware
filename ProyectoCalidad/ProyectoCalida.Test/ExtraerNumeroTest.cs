﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProyectoCalidad;

namespace ProyectoCalida.Test
{
    [TestClass]
    public class ExtraerNumeroTest
    {
        [TestMethod]
        public void ExtraerNumeroSoloNumeros()
        {
            //Arrage
            const string cadena = "123456789";
            const int esperado = 1;

            //Act
            double optenido = Utilidades.extraerNumero(cadena);

            //Assert
            Assert.AreEqual(esperado, optenido);
        }


        [TestMethod]
        public void ExtraerNumeroConNumerosLetras(){
            //Arrage
            const string cadena = "a2bc56f";
            const int esperado = 2;

            //Act
            double optenido = Utilidades.extraerNumero(cadena);

            //Assert
            Assert.AreEqual(esperado, optenido);
        }

        [TestMethod]
        public void ExtraerNumeroSoloLetras()
        {
            //Arrage
            const string cadena = "abcd";
            const int esperado = 0;

            //Act
            double optenido = Utilidades.extraerNumero(cadena);

            //Assert
            Assert.AreEqual(esperado, optenido);
        }

    }
}
