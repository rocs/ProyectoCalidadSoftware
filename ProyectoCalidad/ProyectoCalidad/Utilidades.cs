﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoCalidad
{
    public class Utilidades
    {

        public static double division(int dividendo, int divisor)
        {
            double result;
            try
            {
                if (divisor != 0)
                {

                    result = dividendo / divisor;
                }
                else
                {
                    result = 0;
                }
            }
            catch (Exception)
            {

                result = 0;
            }

            return result;
        }

        public static int extraerNumero(string cadena)
        {
            int result = 0;
            bool esNumero = false;
            bool primero = true;;
            try
            {
                for (int i = 0; i < cadena.Length; i++)
                {
                    esNumero = char.IsNumber(cadena[i]);
                    if (esNumero && primero)
                    {
                        string o = cadena[i].ToString();
                        result = Convert.ToInt32(o);
                        primero = false;
                    }
                }
            }
            catch (Exception)
            {

                result = 0;
            }
            return result;
        }

    }

    
}
